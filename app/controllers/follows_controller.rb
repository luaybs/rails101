class FollowsController < ApplicationController
  before_action :authenticate_user!

  def create
    to_follow_id = params[:user_id]

    follow_obj = current_user.follows.new(follower_id: current_user.id, following_id: to_follow_id)

    if follow_obj.save
      redirect_to user_path(to_follow_id), flash: {success: "Follow successful!"}
    else
      redirect_to user_path(to_follow_id), flash: {error: "Could not follow!, Reasons: #{follow_obj.errors.full_messages.join(', ')}"}
    end
  end

  def unfollow
    to_unfollow_id = params[:user_id]

    unfollow_obj = current_user.follows.where(following_id: to_unfollow_id, follower_id: current_user.id).first

    if unfollow_obj.destroy
      redirect_to user_path(to_unfollow_id), flash: {success: "Unfollow successful!"}
    else
      redirect_to user_path(to_unfollow_id), flash: {error: "Could not unfollow! Reasons: #{unfollow_obj.errors.full_messages.join(', ')}"}
    end
  end
end