Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  resources :users do
    resources :tweets
    resources :follows, only: [:create]
    delete :unfollow, to: "follows#unfollow"
  end

  root to: "pages#home"
end
