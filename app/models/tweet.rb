class Tweet < ApplicationRecord
  belongs_to :user

  validates :content, length: { maximum: 160 }
  validates :content, presence: true
end
