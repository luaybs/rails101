class TweetsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :update, :edit, :destroy]

  def show
    user_id = params[:user_id]
    user = User.find(user_id)

    @tweet = user.tweets.find(params[:id])

  end

  def index
    user_id = params[:user_id]
    user = User.find(user_id)

    # Get request
    @tweets = user.tweets.all

    respond_to do |format|
      format.html
      format.json { render json: @tweets }
    end
  end

  def new
    # Get request
    @tweet = current_user.tweets.new
  end

  def create
    # Post request
    @tweet = current_user.tweets.new(tweet_params)

    respond_to do |format|
      if @tweet.save
        format.html { redirect_to user_tweet_path(current_user, @tweet) }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def destroy
    @tweet = current_user.tweets.find(params[:id])

    if @tweet.destroy
      redirect_to user_tweets_path(current_user)
    end
  end

  def edit
    @tweet = current_user.tweets.find(params[:id])
  end

  def update
    @tweet = current_user.tweets.find(params[:id])

    if @tweet.update(tweet_params)
      redirect_to user_tweets_path(current_user)
    end
  end

  protected
  def tweet_params
    params.require(:tweet).permit(:content)
  end
end
