class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update]


  def index
    @users = User.all

    respond_to do |format|
      format.html
      format.json { render json: @users }
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
  end

  def update
    if current_user.update(user_params)
      redirect_to user_path(current_user)
    else
      render :edit
    end
  end

  protected

  def user_params
    params.require(:user).permit(:first_name,
                                 :last_name,
                                 :email,
                                 :avatar)
  end
end