class Follow < ApplicationRecord
  belongs_to :follower, class_name: "User", foreign_key: "follower_id"
  belongs_to :following, class_name: "User", foreign_key: "following_id"

  validate :follow_uniqueness

  def follow_uniqueness
    follows = Follow.where(follower_id: self.follower_id, following_id: self.following_id)

    if follows.count >= 1
      errors.add(:base, "you can't follow the same person more than once.")
    end
  end
end