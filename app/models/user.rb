class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tweets, :dependent => :destroy

  has_many :follows, foreign_key: "follower_id"
  has_many :followers, class_name: "User", through: :follows, foreign_key: "following_id"
  has_many :followings, class_name: "User", through: :follows, foreign_key: "follower_id"

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  validates :email, :first_name, :last_name, presence: true
  validates :email, :format => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
  validate :first_name_must_be_upcase
  # validate :must_have_at_least_one_tweet

  # Custom validations
  def first_name_must_be_upcase
    fn = first_name.first

    unless /[[:upper:]]/.match(fn)
      errors.add(:first_name, "must be capitalized")
    end
  end

  def must_have_at_least_one_tweet
    if tweets.count == 0
      errors.add(:base, "must have at least one tweet.")
    end
  end
end